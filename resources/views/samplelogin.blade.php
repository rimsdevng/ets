
@extends('layouts.auth')


@section('content')

<div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a class="navbar-brand text-white" href="{{ url('/') }}">
                    <img src="{{ url('frontend/images/logo.png') }}" style="max-width: 140px;" alt="">
                    
                 </a>
            </div>
            <div class="modal-body">
                    <p style="text-align:center;"><strong>Login Portal</strong></p> 
                <form action="#" method="post">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Username</label>
                        <input type="text" class="form-control" placeholder=" " name="Name" id="recipient-name" required="">
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-form-label">Password</label>
                        <input type="password" class="form-control" placeholder=" " name="Password" id="password" required="">
                    </div>
                    <div class="right-w3l">
                        <input type="submit" class="form-control" value="Login">
                    </div>
                    <div class="row sub-w3l my-3">
                        <div class="col sub-agile">
                            <input type="checkbox" id="brand1" value="">
                            <label for="brand1" class="text-secondary">
                                <span></span>Remember me?</label>
                        </div>
                        <div class="col forgot-w3l text-right">
                            <a href="#" class="text-secondary">Forgot Password?</a>
                        </div>
                    </div>
                    {{--  <p class="text-center dont-do">Don't have an account?
                        <a href="#register" class="scroll text-dark font-weight-bold">
                            Register Now</a>
                    </p>  --}}
                </form>
            </div>
        </div>
    </div>
@endsection