@extends('layouts.admin')

@section('title')
    Manage Transfer Request
@endsection 

@section('content')


<div class="page-wrapper"> <!-- content -->
    <div class="content container-fluid">
    <div class="page-header">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                    <h5 class="text-uppercase">Request list</h5>
                    @include('notification')

                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <ul class="list-inline breadcrumb float-right">
                        <li class="list-inline-item"><a href="index.html">Home</a></li>
                        <li class="list-inline-item"><a href="index.html">Transfer Request</a></li>
                        <li class="list-inline-item"> Change Employees Branch</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-3">
              
            </div>
            <div class="col-sm-8 col-9 text-right m-b-20">
                <a href="{{ url('/transfer-employee') }}" class="btn btn-primary float-right btn-rounded"><i class="fa fa-plus"></i> Transfer Employee</a>
             
            </div>
        </div>
    <div class="content-page">

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped custom-table datatable">
                        <thead>
                            <tr>
                                {{--  <th style="width:20%;">Name </th>  --}}
                                <th>S/N</th>
                                <th style="width:20%;">Employee Name</th>
                                <th style="width:20%;">Phone</th>
                                {{--  <th>Address</th>  --}}
                                <th>Current Branch</th>
                                {{--  <th>Desired Branch</th>  --}}
                                <th>Status</th>  
                                <th>Date Enrolled</th>  
                                <th class="text-right" style="width:20%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                                @if(count($employee)>0)

                                <?php $count = 1; ?>
                                @foreach($employee as $e)
                            <tr>
                        <td>
                            
                            <?php echo $count;?>  
                        
                        </td>
                        <td>
                         <a href="{{ url('/transfer-employee-detail/'.$e->eid) }}">{{ $e->fname }} <span>{{ $e->sname }}</span></a> 
                        </td>
                        <td> {{  $e->phone }}</td>
                        <td>{{ $e->branch->name }}</td>
                        {{--  <td>{{ $e->reason  }}</td>  --}}
                       
                        {{--  <td>McCarthy Branch</td>  --}}
                        <td>
                            @if($e->status == 'Transferred')
                                <code class="btn-success">{{ $e->status }}</code>                            
                                    @elseif($e->status == 'Awaiting')
                                <code>{{ $e->status }}</code>                            
                            @endif
                        </td>
                        <td>{{  $e->created_at }}</td>
                        <td class="text-right">
                            
                                    <a href="{{ url('transfer-employee-detail/'.$e->eid) }} " class="btn btn-info mb-1 btn-sm">
                                        Transfer 
                                    </a> 

                                  

                                    <button type="submit" data-toggle="modal" data-target="#delete_employee" class="btn btn-danger btn-sm mb-1">
                                    Delete
                                    </button>
                                </td>
                            </tr>
                            
                            <?php $count ++; ?>
                            


                            <div id="delete_employee" class="modal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content modal-md">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Delete Student</h4>
                                            </div>
                                            <form>
                                                <div class="modal-body card-box">
                                                    <p>Are you sure want to delete this?</p>
                                                    <div class="m-t-20"> <a href="#" class="btn btn-white" data-dismiss="modal">Close</a>
                                                        <a href="{{ url('delete-visitor/'.$e->eid) }}" class="btn btn-danger">Delete</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                
                            @endforeach
                      @else
          
                          <tr>
                              <td colspan="7">
                                  <h3 style="color: silver; text-align: center; margin-top: 30px;"> There are no Employee Due for Transfer </h3>
                              </td>
                          </tr>
          
                          @endif
                          
          
                            {{--  other table row  --}}
                            

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--  notification box  --}}
</div>
</div>


@endsection 