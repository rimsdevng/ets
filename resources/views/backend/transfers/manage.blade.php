@extends('layouts.admin')

@section('title')
    Manage Transfer Request
@endsection 

@section('content')


<div class="page-wrapper"> <!-- content -->
    <div class="content container-fluid">
    <div class="page-header">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                    <h5 class="text-uppercase">Request list</h5>
                    @include('notification')

                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <ul class="list-inline breadcrumb float-right">
                        <li class="list-inline-item"><a href="index.html">Home</a></li>
                        <li class="list-inline-item"><a href="index.html">Employee</a></li>
                        <li class="list-inline-item"> Transfer Request</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-3">
              
            </div>
            <div class="col-sm-8 col-9 text-right m-b-20">
                <a href="{{ url('/transfer-employee') }}" class="btn btn-primary float-right btn-rounded"><i class="fa fa-plus"></i> Transfer Employee</a>
            </div>
        </div>
    <div class="content-page">
        {{--  <div class="row filter-row">
            <div class="col-sm-6 col-md-3">
                <div class="form-group custom-mt-form-group">
                    <input type="text"  />
                    <label class="control-label">Visitor ID</label><i class="bar"></i>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group custom-mt-form-group">
                    <input type="text"  />
                    <label class="control-label">Visitor Name</label><i class="bar"></i>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="form-group custom-mt-form-group">
                    <input type="text"  />
                    <label class="control-label">Mobile</label><i class="bar"></i>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <a href="#" class="btn btn-success btn-block mt-3 mb-2"> Search </a>
            </div>
        </div>  --}}


        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped custom-table datatable">
                        <thead>
                            <tr>
                                {{--  <th style="width:20%;">Name </th>  --}}
                                <th>S/N</th>
                                <th style="width:20%;">Employee Name</th>
                                <th style="width:20%;">Reason</th>
                                {{--  <th>Address</th>  --}}
                                <th>current Branch</th>
                                <th>Desired Branch</th>
                                <th>Status</th>  
                                <th>Date</th>  
                                <th class="text-right" style="width:20%;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                                @if(count($transfers)>0)

                                <?php $count = 1; ?>
                                @foreach($transfers as $transfer)
                            <tr>
                        <td>
                            
                            <?php echo $count;?>  
                        
                        </td>
                        {{--  <a href="{{ url('/visitor-details/'.$transfer->tid) }}">{{ $transfer->fname }} <span>{{ $transfer->sname }}</span></a>  --}}
                        <td>{{ $transfer->fullname }}</td>
                        <td>{{ $transfer->reason  }}</td>
                        <td>Lapaz Branch</td>
                        {{  $transfer->phone }}
                        <td>McCarthy Branch</td>
                        <td>
                            @if($transfer->status == 'Approved')
                                <code class="btn-success">{{ $transfer->status }}</code>                            
                                    @elseif($transfer->status == 'Pending')
                                <code>{{ $transfer->status }}</code>                            
                            @endif
                        </td>
                        <td>{{  $transfer->created_at }}</td>
                        <td class="text-right">
                            
                                    {{--  <a href="{{ url('get-edit-transfer/'.$transfer->tid) }} " class="btn btn-info mb-1 btn-sm">
                                        View Details
                                    </a>  --}}

                                  

                                    @if($transfer->status == 'Pending')
                                                
                                    <span><a class="btn btn-success" href="{{url('approve-request/'.$transfer->tid)}}"><i class="ti-pencil-alt color-success"></i> Approve</a></span>
                                        @else
                                        
                                        <span><a class="btn btn-info" href="{{url('disapprove-request/'.$transfer->tid)}}"><i class="ti-pencil-alt color-success"></i>Disapprove</a></span>

                                    @endif

                                    <button type="submit" data-toggle="modal" data-target="#delete_employee" class="btn btn-danger btn-sm mb-1">
                                    Delete
                                    </button>
                                </td>
                            </tr>
                            
                            <?php $count ++; ?>
                            
                            @endforeach
                      @else
          
                          <tr>
                              <td colspan="7">
                                  <h3 style="color: silver; text-align: center; margin-top: 30px;"> There are no transfers Record Found </h3>
                              </td>
                          </tr>
          
                          @endif
                          
          
                            {{--  other table row  --}}
                            

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--  notification box  --}}
</div>
</div>
 <div id="delete_employee" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-md">
            <div class="modal-header">
                <h4 class="modal-title">Delete Student</h4>
            </div>
            <form>
                <div class="modal-body card-box">
                    <p>Are you sure want to delete this?</p>
                    <div class="m-t-20"> <a href="#" class="btn btn-white" data-dismiss="modal">Close</a>
                        <a href="{{ url('delete-visitor/'.$transfer->tid) }}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection 