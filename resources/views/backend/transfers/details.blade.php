@extends('layouts.admin')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }

        .select-car{
            width: 100%;
            height: 50px;
            border-radius: 3px;
            border-color: gainsboro;
        }

        .apply-text{
            width:91% !important;
        }
</style>


<div class="row mt-5 ml-5 mb-10">
    <div class="col-xl-10 mb-10 mb-xl-0">
            @include('notification')
            <h3>Transfer Employee</h3>
          <div class="card shadow">

            <form method="POST" action="{{ url('post-transfer-employee/'.$employee->eid) }}" enctype="multipart/form-data">
                    {{csrf_field()}}
                <div class="row">

                    <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                            <label for="fname">First Name:</label>
                        <input type="text" class="form-control form-control-alternative" id="exampleFormControlInput1" value="{{ $employee->fname }}" readonly>
                        </div>
                    </div>

                    <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                            <label for="lname">Last Name:</label>
                        <input type="text" class="form-control form-control-alternative" id="exampleFormControlInput1" value="{{ $employee->lname }}" readonly>
                        </div>
                    </div>
                        
                    <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                        <label for="dob">Date of Birth:</label>
                        <input type="text" class="form-control form-control-alternative" value="{{ $employee->dob }}" title="Date of Birth" readonly>
                        </div>
                    </div>

                    <div class="col-sm-5 mt-5 ml-5">
                        <div class="form-group">
                                <label for="gender">Date of Birth:</label>
                                <input type="text" class="form-control form-control-alternative" value="{{ $employee->gender }}" title="Gender" readonly>  
                        </div>
                    </div>

                  
            
                

                   

                    <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                          

                            <label for="gender">Designation:</label>
                            <input type="text" class="form-control form-control-alternative" value="{{ $employee->designation->name }}" title="Designation" readonly>  
                        </div>

                            
                    </div>  
                    <div class="col-md-5 ml-5">
                            <div class="form-group">
                                    <label for="gender">Date Employed:</label>

                                    <input type="text" class="form-control form-control-alternative" id="exampleFormControlInput1" title="Date of Employement" value="{{ $employee->employeed_date }}" readonly>
                                </div>
                            </div>
                    </div> 

                          

                    <div class="col-md-5 mt-5 ml-5">
                            <div class="form-group">
                              
    
                                <label for="gender">Current Branch:</label>
                                <input type="text" name="currentBranch" class="form-control form-control-alternative" value="{{ $employee->branch->name }}" title="Current Branch" readonly>  
                            </div>
    
                                
                        </div> 
                              

                    <div class="col-md-5 mt-5 ml-5">
                            <div class="form-group">
                              
    
                                <label for="gender">Address:</label>
                                <input type="text" class="form-control form-control-alternative" value="{{ $employee->address}}" title="Address" readonly>  
                            </div>
    
                                
                        </div> 
                    
                    <hr>

                    <div class="col-md-10 ml-5">
                            <div class="form-group">
                                    <label for="Branch">Select New Branch:</label>

                                    <select class="form-control from-control-alternative" aria-labelledby="Branch" name="bid" value="{{ old('bid') }}">
                                        <option>--Select Branch--</option>
                                    @foreach ($branch as $b )   
                                        <option value="{{ $b->bid }}">{{ $b->name }}</option>
                                    @endforeach
                                    
                                    </select>
                            </div>
                    </div>

                   

                </div>
                
                
                <div class="row">
                
                    
                 
                    
                   
                    <div class="col-md-11 ml-5">
                        <div class="form-group">
                                <textarea name="reason" class="form-control apply-text" id="exampleFormControlTextarea1" rows="3" col="8" placeholder="Reason for transfer"></textarea>
                        </div>
                    </div>
                </div>

                
                <div class="col-md-10 ml-5 mt-2 mb-3">
                    <button class="btn btn-primary" type="submit">Transfer Employee</button>
                    <button class="btn btn-danger" type="submit">Cancel</button>
                </div>   
            </form>
        </div>
    </div>
</div>

@endsection 