@extends('layouts.app')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }

        .select-car{
            width: 100%;
            height: 40px;
            border-radius: 3px;
            border-color: gainsboro;
        }
</style>


<div class="row mt-5 ml-5">
    <div class="col-xl-8 mb-5 mb-xl-0">
            @include('notification')
            <h3>Add Position Information</h3>
          <div class="card shadow">
            
            <form method="POST" action="{{ url('post-designation') }}" >
                    {{csrf_field()}}
                <div class="row">
                 <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="name" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Name of Position">
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-12 ml-5">
                    <div class="form-group">
                       <textarea name="description" id="" cols="50" rows="3"></textarea>
                    </div>
                </div>
                </div>
                <div class="col-md-10 ml-5 mb-3">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <button class="btn btn-danger" type="submit">Cancel</button>
                </div>   
      </form>
     </div>
  </div>
</div>

@endsection 