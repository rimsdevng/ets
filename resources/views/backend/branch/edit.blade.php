@extends('layouts.admin')


@section('content')

<section class="content">
        <div class="container-fluid">
           
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2 class="card-inside-title">

                                <small>Kindly fill in Branch Information</small>
                            </h2>
                            
                        </div>
                        <div class="body">
                          

                           
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
          
           
          
            
        
        </div>
    </section>
    
    @include('notification')

    <h2>Hello add branch here</h2>

    <div class="row clearfix">
        <div class="col-sm-8">
         <form method="post" action="{{url('/post-edit-branch/'.$branch->bid)}}">
            {{csrf_field()}}
            <div class="form-group">
                <div class="form-line">
                    <input type="text" class="form-control" name="name" value="{{ $branch->name }}" placeholder="Name" />
                </div>
            </div>

            <div class="form-group">
                <div class="form-line">
                    <textarea class="form-control" name="address" cols="30" rows="2">{{ $branch->address }}
                    </textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect pull-right">Add</button>
         </form>
        </div>
    </div>

@endsection