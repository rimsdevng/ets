@extends('layouts.admin')

@section('content')
<style>
    .dashboard, dash-count{
        padding-top: 0% !important;
    }
</style>
<div class="tab-main">
    {{-- tabs-inner --}}

    @if(Auth::check() and Auth::user()->role !== 'Employee')
        <div class="tab-inner">
            <div id="tabs" class="tabs">
                <h2 class="inner-title">Dashboard</h2>
                    {{-- <div class="graph"> --}}
                        {{-- <nav>
                            <ul>
                                <li class="tab-current"><a href="#section-1" class="icon-shop"><span>Shop</span></a></li>
                                <li><a href="#section-2" class="icon-cup"><span>Men</span></a></li>
                                <li><a href="#section-3" class="icon-food"><span>Women</span></a></li>
                                <li><a href="#section-4" class="icon-lab"><span>Kids</span></a></li>
                                <li><a href="#section-5" class="icon-truck"><span>Order</span></a></li>
                            </ul>
                        </nav> --}}
                        {{-- End of Content --}}
                    {{-- </div> --}}
                                <div class="content tab dashboard">
                                            <section id="section-1" class="content-current dash-count">

                                                <div class="mediabox">
                                                        <i class="fa fa-file-text-o"></i>
                                                        <div class="content-top-1">
                                                            <div class="col-md-4 top-content">
                                                                <h5>Today Requests</h5>
                                                                <label>{{ count($transfer) }}</label>
                                                            </div>
                                                                <div class="clearfix"> </div>
                                                        </div>
                                                </div>
                                                <div class="mediabox">
                                                        <i class="fa fa-users"></i>
                                                        <div class="content-top-1">
                                                            <div class="col-md-4 top-content">
                                                                <h5>Employees</h5>
                                                                <label>{{ count($employees) }}</label>
                                                            </div>
                                                                <div class="clearfix"> </div>
                                                        </div>
                                                </div>
                                                <div class="mediabox">
                                                    <i class="fa fa-home"></i>
                                                    <div class="content-top-1">
                                                            <div class="col-md-4 top-content">
                                                                <h5>Branches</h5>
                                                                <label>{{ count($branches) }}</label>
                                                            </div>
                                                                <div class="clearfix"> </div>
                                                    </div>
                                                </div>
                                            </section>
                                            {{-- <section id="section-2">
                                                <div class="mediabox">
                                                    <i class="fa fa-suitcase"></i>
                                                    <h3>Various versions</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                                <div class="mediabox">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    <h3>Industry's standard</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                                <div class="mediabox">
                                                    <i class="fa fa-sitemap"></i>
                                                    <h3>Into electronic</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                            </section>
                                            <section id="section-3">
                                                <div class="mediabox">
                                                    <i class="fa fa-sitemap"></i>
                                                    <h3>Into electronic</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                                <div class="mediabox">
                                                    <i class="fa fa-suitcase"></i>
                                                    <h3>Various versions</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                                <div class="mediabox">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    <h3>Industry's standard</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                            </section>
                                            <section id="section-4">
                                                <div class="mediabox">
                                                    <i class="fa fa-suitcase"></i>
                                                    <h3>Various versions</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                                <div class="mediabox">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    <h3>Industry's standard</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                                <div class="mediabox">
                                                    <i class="fa fa-sitemap"></i>
                                                    <h3>Into electronic</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                            </section>
                                            <section id="section-5">
                                                <div class="mediabox">
                                                    <i class="fa fa-sitemap"></i>
                                                    <h3>Into electronic</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                                <div class="mediabox">
                                                    <i class="fa fa-suitcase"></i>
                                                    <h3>Various versions</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                                <div class="mediabox">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    <h3>Industry's standard</h3>
                                                    <p>Chickweed okra pea winter purslane coriander yarrow sweet pepper radish garlic brussels sprout groundnut summer purslane earthnut pea tomato spring onion azuki bean gourd. </p>
                                                </div>
                                            </section> --}}
                                        
                                </div>
                {{-- tabs --}}
        </div>
    @endif
        <script src="{{ url('backend/js/cbpFWTabs.js') }}"></script>
        <script>
            new CBPFWTabs( document.getElementById( 'tabs' ) );
        </script>
        @if(Auth::check() and Auth::user()->role !== 'Employee')

        <div class="col-md-6 graph-2">
            <h3 class="inner-tittle">Recent Employees Added</h3>
            <div class="panel panel-primary">
                <div class="panel-heading">Details</div>
                    <div class="panel-body ont">
                      
                    @if(count($employee)>0)
                        @foreach ($employee as $e)                    
                            <p>{{ $e->fname }}</p> <p>{{ $e->lname }}</p>
                        @endforeach

                        @else
                        <strong>No Employee Added Today</strong>
                    @endif

                    
                    </div>
            </div>
        </div>

        <div class="col-md-6 graph-2 second">
            <h3 class="inner-tittle">Recent Transfer Request </h3>
                <div class="panel panel-primary two">
                    <div class="panel-heading">List</div>
                        <div class="panel-body ont two">
                            
                            
                            @if(count($transfer)>0)
                            @foreach ($transfer as $t)                    
                                <p>{{ $t->fname }}</p> <p>{{ $t->reason }}</p>
                            @endforeach
    
                            @else
                            <strong>No Transfer Request Today</strong>
                        @endif
                        </div>
                            
                </div>
        </div>
        @endif

        @if(Auth::check() and Auth::user()->role == 'Employee')

        <div class="col-md-6 graph-2">
            <h3 class="inner-tittle">Transfer Status</h3>
            <div class="panel panel-primary">
                <div class="panel-heading">Welcome: {{ Auth::user()->name }}</div>
                    <div class="panel-body ont"><p>Your Application  <code>ID : 1</code> is still <strong style="color:orangered;">Pending</strong> </p>
                </div>
            </div>
        </div>
        @endif
        <div class="clearfix"> </div>

        
     </div>
    

     {{-- <div class="col-md-7 mid-content-top">
            <div class="middle-content">
                <h3>Latest Products
                </h3>
                <!-- start content_slider -->
                    <div id="owl-demo" class="owl-carousel text-center owl-theme" style="opacity: 1; display: block;">
                        <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1414px; left: 0px; display: block; transition: all 800ms ease 0s; transform: translate3d(-202px, 0px, 0px);"><div class="owl-item" style="width: 101px;"><div class="item">
                            <img class="lazyOwl img-responsive" alt="name" src="{{ url('backend/images/na.jpg') }}" style="display: block;">
                        </div></div><div class="owl-item" style="width: 101px;"><div class="item">
                            <img class="lazyOwl img-responsive" alt="name" src="{{ url('backend/images/na1.jpg')}}" style="display: block;">
                        </div></div><div class="owl-item" style="width: 101px;"><div class="item">
                            <img class="lazyOwl img-responsive" alt="name" src="{{ url('backend/images/na2.jpg')}}" style="display: block;">
                        </div></div><div class="owl-item" style="width: 101px;"><div class="item">
                            <img class="lazyOwl img-responsive" alt="name" src="{{ url('backend/images/na.jpg')}}" style="display: block;">
                        </div></div><div class="owl-item" style="width: 101px;"><div class="item">
                            <img class="lazyOwl img-responsive" alt="name" src="{{ url('backend/images/na1.jpg')}}" style="display: block;">
                        </div></div><div class="owl-item" style="width: 101px;"><div class="item">
                            <img class="lazyOwl img-responsive" alt="name" src="{{ url('backend/images/na2.jpg')}}" style="display: block;">
                        </div></div><div class="owl-item" style="width: 101px;"><div class="item">
                            <img class="lazyOwl img-responsive" alt="name" src="{{ url('backend/images/na.jpg')}}" style="display: block;">
                        </div></div></div></div>
                        
                        
                        
                        
                        
                        
                        
                    <div class="owl-controls clickable"><div class="owl-pagination"><div class="owl-page active"><span class=""></span></div><div class="owl-page"><span class=""></span></div></div></div></div>
            </div>
            <!--//sreen-gallery-cursual---->
            <!-- requried-jsfiles-for owl -->
            <link href="{{ url('backend/css/owl.carousel.css') }}" rel="stylesheet">
            <script src="{{ url('backend/js/owl.carousel.js') }}"></script>
                <script>
                    $(document).ready(function() {
                        $("#owl-demo").owlCarousel({
                            items : 3,
                            lazyLoad : true,
                            autoPlay : true,
                            pagination : true,
                            nav:true,
                        });
                    });
                </script>
            <!-- //requried-jsfiles-for owl -->
    </div> --}}
</div>
@endsection('content')