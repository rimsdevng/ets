@extends('layouts.admin')

@section('content')


   
    
<!-- ============================================================== -->
<!-- 						Content Start	 						-->
<!-- ============================================================== -->
        
<div class="row page-header">
<div class="col-lg-6 align-self-center ">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Add Staff</li>		
    </ol>
</div>
</div>


<div class="row">
    <div class="col-sm-8 ml-5">
        @include('notification')
        <div class="card">
            <div class="card-header card-default">
               Add Staff Information
            </div>
            <div class="card-body">
                <form method="post" id="form" action="{{ url('/post-add-user') }}">
                    {{ csrf_field() }}
                    <div class="form-group ">
                        
                        <label for="name">Full Name</label>

                        <input id="name" type="text" class="form-control form-control-rounded" name="name" required>
                    </div>
                
                    <div class="form-group">
                        
                        <label for="email">Email</label>

                        <input id="email" type="email" class="form-control form-control-rounded" name="email" required>
                        
                    </div>

                    {{--  <div class="form-group">
                        <label for="confirmpassword">Confirm Password</label>

                        <input id="confirmpassword" type="password" class="form-control form-control-rounded" name="confirmpassword" required>

                        
                    </div>  --}}
                    <small class="text-muted">Kindly Click to save users information.</small>
                        
                    <button type="submit" class="btn btn-primary margin-l-5 mx-sm-3">Add</button>
                    <a href="{{ url('/') }}" class="btn btn-warning">Cancel</a>
                    
                    
                </form>


            </div>
        </div>
    </div>
</div>

@endsection