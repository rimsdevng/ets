@extends('layouts.app')


@section('content')

<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Add Departments</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2 class="card-inside-title">

                                <small>Kindly fill in Department Information</small>
                            </h2>
                            
                        </div>
                        <div class="body">
                            @include('notification')
                            <div class="row clearfix">
                                <div class="col-sm-8">
                                <form method="post" action="{{url('/post-department')}}">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" placeholder="Name" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="slug" placeholder="Slug" />

                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect pull-right">Add</button>
                                 </form>
                                </div>
                            </div>

                           
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
          
           
          
            
        
        </div>
    </section>
@endsection