
<div class="header_bg">
    <div class="header">
        <div class="head-t">
            <div class="logo">
                <a href="{{ url('/home') }}">
                        <img src="{{ url('frontend/images/logo.png') }}" style="max-width: 140px;" alt="">
                </a>
                
            </div>
                <!-- start header_right -->
            <div class="header_right">
            
                <div class="rgt-bottom">
                        <?php echo date('Y-M-D H:M, A')?>
                    <div class="create_btn pull-right">

                        <a href="{{ url('/logout') }}">Signout</a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            
            </div>
        <div class="clearfix"> </div>
    </div>
</div>
</div>