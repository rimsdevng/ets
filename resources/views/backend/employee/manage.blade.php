@extends('layouts.admin')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }

        .select-car{
            width: 100%;
            height: 40px;
            border-radius: 3px;
            border-color: gainsboro;
        }
</style>



<div class="row mt-5 ml-5">
    <div class="col-xl-8 mb-5 mb-xl-0">
            <h3>Manage Employees</h3>
     <div class="card shadow">

          
     </div>
  </div>
</div>


<div class="container-fluid mt--7">
    <!-- Table -->
    <div class="row">
      <div class="col">
        <div class="card shadow">
          <div class="card-header border-0">
            @include('notification')
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                  <th scope="col">S/N</th>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  {{--  <th scope="col">Category</th>  --}}
                  <th scope="col">Gender</th>
                  <th scope="col">Date Of Birth</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Designation</th>  
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                  {{--  the foreach was here  --}}

                  @if(count($staff)>0)

                  <?php $count = 1; ?>

                  @foreach($staff as $s)
                  
                <tr>

                  <th scope="row">

                      <div class="media align-items-center">
                          <a href="#" class="avatar rounded-circle mr-3">
                              {{--  the counting numbers below  --}}
                              <?php echo $count;?>   
                          </a>
                      {{--  <div class="media-body">
                        <span class="mb-0 text-sm"></span>
                      </div>  --}}
                    </div>
                  </th>
                  <td>
                      {{ $s->fname }} {{ $s->lname }}
                  </td>
                  <td>
              
                    {{$s->email}}     
                 </td>
                  <td>
                    {{ $s->gender }}
                  </td>
                  <td>
                    {{ $s->dob }}
                  </td>
                  
                  <td>
                    {{$s->phone }}
                  </td> 
                  <td>
                    {{ $s->designation->name }}
                  </td>
                  
                  <td>
                    <a class="btn btn-default" href="{{ url('edit-employee/'.$s->eid) }}">Edit</a>
                    <a class="btn btn-danger" href="{{ url('delete-employee/'.$s->eid) }}">Delete</a>
                       

                  </td>
                </tr>

                <?php $count ++; ?>
               
                  @endforeach
                  @else

                      <tr>
                          <td colspan="7">
                              <h3 style="color: silver; text-align: center; margin-top: 30px;"> There are no Staff Record available </h3>
                          </td>
                      </tr>


                  @endif


{{-- The dump was here --}}

              </tbody>
            </table>
          </div>
          <div class="card-footer py-4">
            <nav aria-label="...">
           
              {{ $staff->links() }}

            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Dark table -->
    
   @include('footer')
  </div>

  
@endsection 