@extends('layouts.admin')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }

        .select-car{
            width: 100%;
            height: 50px;
            border-radius: 3px;
            border-color: gainsboro;
        }

        .apply-text{
            width:91% !important;
        }
</style>


<div class="row mt-5 ml-5 mb-10">
    <div class="col-xl-10 mb-10 mb-xl-0">
            @include('notification')
            <h3>Edit Employee Data</h3>
          <div class="card shadow">

            

            <form method="POST" action="{{ url('post-edit-employee/'.$employee->eid) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
    
                <div class="row">

                    <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="fname" value="{{ $employee->fname }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="First Name">
                        </div>
                    </div>

                    <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="lname" value="{{ $employee->lname }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Last Name">
                        </div>
                    </div>
                        
                    <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                        <input type="date" name="dob" value="{{ $employee->dob }}" class="form-control form-control-alternative" placeholder="dob" title="Date of Birth">
                        </div>
                    </div>

                    <div class="col-sm-5 mt-5 ml-5">
                        <div class="form-group">
                                <select class="form-control from-control-alternative" name="gender" default="Gender" type="text">
                                        <option>Gender</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                </select>
                        </div>
                    </div>

                  
            
                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="phone" value="{{ $employee->phone }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="phone">
                        </div>
                    </div>

                   

                    <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                            <select class="form-control from-control-alternative" title="Select Designation" aria-labelledby="Designation" name="deid" value="{{ $employee->deid }}">

                            @foreach ($designation as $d )   
                                <option value="{{ $d->deid }}">{{ $d->name }}</option>
                            @endforeach
                            
                            </select>
                        </div>

                            
                    </div>


                    <div class="col-md-5 ml-5">
                            <div class="form-group">
                                    {{-- <label for="Branch">Select Branch Details:</label> --}}
                                    <select class="form-control from-control-alternative" aria-labelledby="Branch" name="bid" value="{{ old('bid') }}">
                                        <option>--Select Branch--</option>
                                    @foreach ($branch as $b )   
                                        <option value="{{ $b->bid }}">{{ $b->name }}</option>
                                    @endforeach
                                    
                                    </select>
                            </div>
                    </div>

                    <div class="col-md-5 ml-5">
                            <div class="form-group">
                                    <input type="date" name="employeed_date" value="{{ $employee->employeed_date }}" class="form-control form-control-alternative" id="exampleFormControlInput1" title="Date of Employement" placeholder="Employeed Date">
                                </div>
                            </div>
                    </div> 

                </div>
                
                
                <div class="row">
                
                    
                 
                    
                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="email" name="email" value="{{ $employee->email }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="mail@example.com">
                        </div>
                    </div>
                    
                    
                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="town" value="{{ $employee->town }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="town">
                        </div>
                    </div>
                </div>
              
                <div class="row">
                    

                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="city" value="{{ $employee->city }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="city">
                        </div>
                    </div>

                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="country" value="{{ $employee->country }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="country">
                        </div>
                    </div>

                    {{-- <div class="col-md-10 ml-5">
                            <div class="form-group">
                                <label for="image">Employee Image (optional):</label>
                            <input type="file" name="image" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Add image">
                            </div>
                    </div> --}}
                    <div class="col-md-11 ml-5">
                        <div class="form-group">
                                <textarea name="address" class="form-control apply-text" id="exampleFormControlTextarea1" rows="3" col="8" placeholder="Address"> {{ $employee->address }}</textarea>
                        </div>
                    </div>
                </div>

                
                <div class="col-md-10 ml-5 mt-2 mb-3">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <button class="btn btn-danger" type="submit">Cancel</button>
                </div>   
            </form>
        </div>
    </div>
</div>

@endsection 