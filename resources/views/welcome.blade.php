<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Ecobank - Employee Transfer Application Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="Eco Bank Ghana, Banking in Ghana." />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Custom Theme files -->
    <link href="{{ url('frontend/css/bootstrap.css') }}" type="text/css" rel="stylesheet" media="all">
    <link href="{{ url('frontend/css/style.css') }}" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="{{ url('frontend/css/fontawesome-all.min.css') }}" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <!-- //online-fonts -->
</head>

<body>
    <!-- banner -->
    <div class="banner" id="home">
        <!-- header -->
        <header>

            @include('notification')
            <nav class="navbar navbar-expand-lg navbar-light bg-gradient-secondary pt-3">
                <h1>
                    <a class="navbar-brand text-white" href="{{ url('/') }}">
                       <img src="{{ url('frontend/images/logo.png') }}" style="max-width: 140px;" alt="">
                    </a>
                </h1>
                <button class="navbar-toggler ml-md-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-lg-auto text-center">
                        
                        <li class="nav-item active  mr-3 mt-lg-0 mt-3">
                            <a class="nav-link" href="{{ url('/') }}">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        
                        <li class="nav-item  mr-3 mt-lg-0 mt-3">
                            <a class="nav-link scroll" href="#services">About</a>
                        </li>
                        
                        @if(Auth::check() and Auth::user()->role == 'Employee')
                            <li class="nav-item dropdown mr-3 mt-lg-0 mt-3">
                                <a class="nav-link" href="{{ url('/') }}#register" id="navbarDropdown" role="button" aria-haspopup="true"
                                    aria-expanded="false">
                                    Apply
                                </a>
                            </li>                
                        @endif
                       
                        
                        @if(Auth::check() and Auth::user()->role == 'HR' || Auth::user()->role == 'Admin')

                        <li class="nav-item mr-3 mt-lg-0 mt-3">
                            {{-- <a class="nav-link scroll" href="#contact">Dashboard</a> --}}
                            <a class="btn  ml-lg-2 w3ls-btn" href="{{ url('/home') }}">Dashboard</a>

                        </li>
                        @endif

                        @if(Auth::check() and Auth::user()->role == 'Employee')             
                            <li class="nav-item mr-3 mt-lg-0 mt-3">

                                <a class="btn  ml-lg-2 w3ls-btn" href="{{ url('/home') }}">Profile</a>

                            </li>
                        @endif
                      
                      
                        <li>
                            @if (Route::has('login'))       
                                <div class="top-right links">
                                    @auth
                                        {{--  Previously the link to to dashboard and profie was here  --}}
                                        
                                        @else
                                        <a class="btn  ml-lg-2 w3ls-btn" href="{{ route('login') }}">Login</a>
                                        {{--  @if (Route::has('register'))
                                            <a href="{{ route('register') }}">Register</a>
                                        @endif  --}}
                                    @endauth  
                                </div>
                            @endif                       
                        </li>

                    </ul>
                </div>

            </nav>
        </header>
        <!-- //header -->
        <div class="container">
            <!-- banner-text -->
            <div class="banner-text">
                <div class="slider-info">
                    <h3>Request for your transfer anytime</h3>
                    <a class="btn btn-primary mt-lg-5 mt-3 agile-link-bnr scroll" href="#register" role="button">View More</a>
                </div>
            </div>
        </div>
    </div>

    <!-- register -->
    {{-- @if(Auth::check() and Auth::user()->role == 'Applicant') --}}
    @if(Auth::check() and Auth::user()->role == 'Employee')
        <div class="w3-register py-4" id="register">
            <div class="container py-lg-5">
                <div class="row register-form py-md-5">
                    <!-- register details -->
                    <div class="col-lg-4 register-bottom d-flex flex-column register-right-w3ls">
                        <div class="title-wthree">
                            <h3 class="agile-title">
                                Request
                            </h3>
                            <span></span>
                        </div>

                        <p class="pb-sm-5">We listen to transfer request from our employees.
                            Also, we vets our employees details and transfer them to any branch which seems close to their home address.</p>

                        <h5 class="pt-5 text-capitalize">fill out the form
                            <i class="far fa-hand-point-right"></i>
                        </h5>

                        <p class="font-weight-bold text-capitalize">to request for a
                            <span>transfer</span>
                        </p>
                    </div>
                    <div class="offset-lg-2"></div>
                    <div class="col-lg-6 wthree-form-left py-sm-5">
                        <!-- register form grid -->
                        <div class="register-top1">
                            <form action="{{ url('/post-add-request') }}" method="post" class="register-wthree">
                                    {{csrf_field()}}
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                            <label class="mb-0">
                                                <span class="fas fa-user"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-10">
                                            <label>
                                                    Full name
                                            </label>
                                            <input class="form-control" type="text" value="{{ Auth::user()->name }}" name="fullname" required="">
                                        </div>
                                        {{--  <div class="col-md-5">
                                            <label>
                                                Last name
                                            </label>
                                            <input class="form-control" type="text" placeholder="Full Name" name="name" required="">
                                        </div>  --}}
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                            <label class="mb-0">
                                                <span class="fas fa-envelope-open"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-10">
                                            <label>
                                                Email
                                            </label>
                                            <input class="form-control" type="email" value="{{ Auth::user()->email }}" name="email" required="">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                                <label class="mb-0">
                                                    <span class="fas fa-clock"></span>
                                                </label>
                                            </div>
                                            <div class="col-md-10">
                                                <label>
                                                 Desired  Period 
                                                </label>
                                                <input class="form-control" type="date" placeholder="When do you want to be transfered" name="transfer_request_date" required="">
                                            </div>
                                          
                                        </div>
                                </div>


                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                            <label class="mb-0">
                                                <span class="fas fa-comment"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-10">
                                            <label>
                                                Reason
                                            </label>
                                            <textarea name="reason" class="form-control" id="" cols="30" rows="10" placeholder="Give detail reason"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                            <label class="mb-0">
                                                <span class="fas fa-home"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-10">
                                            <label>
                                                Current branch
                                            </label>
                                            
                                            {{--  <select name="bid" id="">
                                                @foreach ($employee as $e )   
                                                <option value="{{ $e->bid }}">{{ $e->bid['name'] }}</option>
                                                @endforeach
                                            </select>  --}}

                                            {{--  value="{{ Auth::user()->bid['name'] }}"\  --}}

                                            <input class="form-control" type="text" name="currentBranch" value="" id="branchid" required="">

                                            
                                                
                                            {{--  <input class="form-control" type="text" value="{{ $userb }}"  name="bid" required="">
                                              --}}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-2 d-md-flex align-items-end justify-content-end px-md-0">
                                            <label class="mb-0">
                                                <span class="fas fa-home"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-10">
                                            <label>
                                                Select desired branch
                                            </label>
                                            
                                            <select name="bid" id="">
                                                @foreach ($branch as $b )   
                                                <option value="{{ $b->bid }}">{{ $b->name }}</option>
                                                @endforeach
                                            </select>

                                                

                                        </div>
                                    </div>
                                </div>    

                                <div class="row mt-lg-5 mt-3">
                                    <div class="offset-2"></div>
                                    <div class="col-md-10">
                                        <button type="submit" class="btn btn-agile btn-block w-100">apply</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--  //register form grid ends here -->
                    </div>

                </div>
                <!-- //register details container -->
            </div>
        </div>
    @endif

    <!-- //register -->

    <!-- services -->
    <div class="agileits-services py-5" id="services">
        <div class="container py-sm-5">
            <div class="title-wthree text-center">
                <h3 class="agile-title   text-white">
                    Transfer Process
                </h3>
                <span></span>
            </div>
            <div class="agileits-services-row row  py-sm-5">
                <div class="col-lg-4">
                    <div class="agileits-services-grids">
                        <img src="images/i1.jpg" alt="" class="img-fluid rounded-circle" />
                        <h4 class="sec-title">Login
                        </h4>
                        <span></span>
                        <p>To request for transfer, and employee should login to the system with credientials provided by the HR. Contact Management for details.</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="agileits-services-grids mt-lg-0 mt-5">
                        <img src="images/i3.jpg" alt="" class="img-fluid rounded-circle" />
                        <h4 class="sec-title">Apply
                        </h4>
                        <span></span>
                        <p>Employee should fill in reason why they would want to be transfered to the requested branch.</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="agileits-services-grids mt-lg-0 mt-5">
                        <img src="images/i2.jpg" alt="" class="img-fluid rounded-circle" />
                        <h4 class="sec-title">Approve
                        </h4>
                        <span></span>
                        <p>Wait for management approval for the transfer request. For further enquiry contact the HR Department.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //services -->
    <!-- //container -->
    <!-- //banner -->
    <!-- process -->
  
    <!-- //process -->
    <!--partners  -->
    
    <!-- //partners -->
    <!-- contact -->
  
    <!-- //contact -->
    <footer id="footer" class="text-sm-left text-center">
        <div class="container py-4 py-sm-5">
            <h2>
                <a class="navbar-brand text-white" href="{{ url('/') }}">
                    <img src="{{ url('frontend/images/logo.png') }}" style="max-width: 140px;" alt="">
                </a>
            </h2>
            
            <hr>
            <div class="d-flex justify-content-between pt-lg-5   footer-bottom-cpy">
                <div class="cpy-right text-center">
                    <p style="color:#0f6284;">© <? php echo date('Y')?> Ecobank. All rights reserved | Design by Ecobank
                        
                    </p>
                </div>
                <div class="social-icons pb-md-0 pb-4">
                    <ul class="social-iconsv2 agileinfo text-center">
                        <li class="ml-lg-5">
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- login  -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" method="post">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Username</label>
                            <input type="text" class="form-control" placeholder=" " name="Name" id="recipient-name" required="">
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-form-label">Password</label>
                            <input type="password" class="form-control" placeholder=" " name="Password" id="password" required="">
                        </div>
                        <div class="right-w3l">
                            <input type="submit" class="form-control" value="Login">
                        </div>
                        <div class="row sub-w3l my-3">
                            <div class="col sub-agile">
                                <input type="checkbox" id="brand1" value="">
                                <label for="brand1" class="text-secondary">
                                    <span></span>Remember me?</label>
                            </div>
                            <div class="col forgot-w3l text-right">
                                <a href="#" class="text-secondary">Forgot Password?</a>
                            </div>
                        </div>
                        <p class="text-center dont-do">Dont have an account?
                            <a href="#register" class="scroll text-dark font-weight-bold">
                                Register Now</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- //login -->
    <!-- //footer -->
    <!-- js -->
    <script src="{{ url('frontend/js/jquery-2.2.3.min.js') }}"></script>
    <!-- //js -->
    <!-- script for password match -->
    <script>
        window.onload = function () {
            document.getElementById("password1").onchange = validatePassword;
            document.getElementById("password2").onchange = validatePassword;
        }

        function validatePassword() {
            var pass2 = document.getElementById("password2").value;
            var pass1 = document.getElementById("password1").value;
            if (pass1 != pass2)
                document.getElementById("password2").setCustomValidity("Passwords Dont Match");
            else
                document.getElementById("password2").setCustomValidity('');
            //empty string means no validation error
        }
    </script>
    <!-- script for password match -->
    <!-- start-smooth-scrolling -->
    <script src="{{ url('frontend/js/move-top.js') }}"></script>
    <script src="{{ url('frontend/js/easing.js') }}"></script>
    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });


    </script>
    @if(Auth::check() and Auth::user()->role == 'Employee')

    <script>
        //get bbranch start
        $(document).ready(function ($) {

            var bid = {{ Auth::user()->bid  }}

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: `{{ URL::to('/branchName') }}/${bid}`,
                dataType: 'json',
                method: 'GET',
                success: function (data) {
                    console.log(data);

                    $('#branchid').val(data.name);
                },
                error :  function(error) {
                    console.log(error)
                }
            })

        });

        //get branch end
    </script>
    @endif
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
        $(document).ready(function () {
            /*
            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
            };
            */

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <script src="{{ url('frontend/js/SmoothScroll.min.js') }}"></script>
    <!-- //smooth-scrolling-of-move-up -->
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ url('frontend/js/bootstrap.js') }}"></script>
</body>

</html>














































{{-- <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Employee  Management System
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html> --}}
