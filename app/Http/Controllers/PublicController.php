<?php

namespace App\Http\Controllers;
use App\branch;
use App\department;
use App\employee;
use App\user;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    //

    public function getWelcome(){
        $employee = employee::all();
        $user = auth()->user();
        // $user = user::join('users.bid', '=', 'branches.bid')->select('branch.name')->get();
        $branch = branch::all();
        return view ('welcome',[

            
            'branch'=> $branch,
            'employee' => $employee,
            'user' => $user


        ]);
    }

    public function getBranch($bid){

        $branch = branch::findOrFail($bid);

        return response()->json($branch, 200);
    }

   

}
