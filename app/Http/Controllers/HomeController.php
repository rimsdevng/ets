<?php

namespace App\Http\Controllers;

use App\User;
use App\employee;
use App\branch;
use App\designation;
use App\transfer;
use App\department;
use App\transferhistory;
use Faker\Provider\Company;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     /*
       TODO:
    -  Create the page to post the booking to single table then use confirm status to sperate the date to different tables
    -  Try inserting into different tables on creation instead of using confirmation method
    -  Then try to debug the application and see why it's not inserting to database
      */
    public function index()
    {
      
		// $todaysales = sale:: where( DB::raw('DATE(created_at)') ,'=', Carbon::today());
        // ->count()
        // where( DB::raw('DATE(created_at)') ,'=', Carbon::today());
        $employee = employee:: whereDate('created_at', Carbon::today())->get();
        $transfer = transfer:: whereDate('created_at', Carbon::today())->get();

        $branches = branch:: all();
        $employees = employee::all();
        return view('backend.home',[
			
            'employee' => $employee,
            'transfer' => $transfer,
            'employees' => $employees,
            'branches' => $branches

        ]);
        
    }
  
    
    public function getAddBranch(){

        return view ('backend.branch.add');
    }

    public function postAddBranch(Request $request){

        $branch = new branch();
		$branch->name = $request->input('name');
		$branch->address = $request->input('address');
		$status = $branch->save();

		if ($status){
            session()->flash('success','branch Created Successfully.');
            return redirect()->back();
		}else{
            session()->flash('error','Something went Wrong, Try again');
            return redirect()->back();
		}

		
    }

    public function manageBranch(){
        $branches = branch::orderBy('created_at','desc')->paginate(10);
        return view('backend.branch.manage', [

            'branches' => $branches
        ]);
    }



    public function editBranch( $bid ) {
        $designation = designation::all();
        $branch = branch::find($bid);
		

		return view('backend.branch.edit',[
            'designation' => $designation,
            'branch' => $branch
		]);
    }
    
    public function postEditBranch(Request $request, $bid){

        $branch  =  branch::findorfail($bid);
        $status = $branch->update($request->all());
        
        if($status)
            
            return redirect('/manage-branch')->with('success' , 'Branch Record Updated successfully');
		else
		    
        return redirect('/manage-branch')->with('error' , 'Sorry something went wrong contact IT');
    }

    public function deleteBranch($bid) {
		$branch = branch::destroy($bid);

		if ($branch){
			session()->flash('success','Branch Deleted Successfully');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
	}

    public function getDepartment(){

        return view ('backend.department.add');
    }

    public function postDepartment(Request $request){

        $department = new department();
		$department->name = $request->input('name');
		$department->address = $request->input('slug');
		$status = $department->save();

		if ($status){
			session()->flash('success','Department Created Successfully.');
		}else{
			session()->flash('error','Something went Wrong, Try again');
		}

		return redirect()->back();
    }

    
    public function addDesignation( ) {
		return view('backend.branch.designation');
	}
    
	public function postDesignation(Request $request) {
		$designation = new designation();
		$designation->name = $request->input('name');
		$designation->description = $request->input('description');

		$status = $designation->save();
		if ($status){
			session()->flash('success','Designation Added.');
		}else{
			session()->flash('error', 'Sorry, Something Went Wrong');
		}
		return redirect()->back();

    }
    
    public function addEmployee(){
        $designation = designation::all();
        $branch = branch::all();
        return view('backend.employee.add', [
            'designation' => $designation,
            'branch' => $branch
        ]);
    }


    public function postEmployee (Request $request) {
        
        try{

        DB::beginTransaction();
        
     
       

		$employee = new employee();
        $employee->deid = $request->input('deid');
        $employee->bid = $request->input('bid');
        $employee->did = $request->input('did');
        $employee->email = strtolower($request->input('email'));
		$employee->fname = $request->input('fname');
		$employee->lname = $request->input('lname');
		$employee->dob  = $request->input('dob');
        $employee->employeed_date = $request->input('employeed_date');
		$employee->gender = $request->input('gender');
		$employee->phone = $request->input('phone');
		$employee->address = $request->input('address');
		$employee->town = $request->input('town');
		$employee->city = $request->input('city');
		$employee->country = $request->input('country');
		$employee->password = bcrypt("Ecobank123");    
        $employee->save();

        $e_fname = str_slug($employee->fname);
        $e_lname = str_slug($employee->lname);

        $fullname = strtoupper($e_fname . " " . $e_lname);
        $user      = new User();
        $user->uid = $employee->eid;
        $user->name = $fullname;
        $user->email = $employee->email;
        $user->bid = $employee->bid;
        $user->password = $employee->password;
        $user->role = 'Employee';
        // $user->status = 'active';
        $user->save();
        
            // insert the details to transfer table and set his status to awaiting 

        
        DB::commit();
        
        // '/manage-staff'
            return redirect()->back()->with('success' , 'Employee Added successfully');
            
        }catch (\Exception $exception){


            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact IT.");

            return redirect()->back();
        }


    }

    public function manageTransferRequests(){
        $designation = designation::all();
        $branch = branch::all();
        $transfers = transfer::all();
        return view('backend.transfers.manage', [
            'designation' => $designation,
            'branch' => $branch,
            'transfers' => $transfers
        ]);
    }

    
    public function getTransferReports(){
        $designation = designation::all();
        $branch = branch::all();
        $transfers = transferhistory::all();
        return view('backend.transfers.reports', [
            'designation' => $designation,
            'branch' => $branch,
            'transfers' => $transfers
        ]);
    }



    public function manageEmployee(){
        $designation = designation::all();

        $staff = employee::orderBy('created_at','desc')->paginate(7);
        return view ('backend.employee.manage',[
            'designation' => $designation,
            'staff' => $staff
        ]);
    }

    public function editEmployee( $eid ) {
        $designation = designation::all();
        $branch = branch::all();
		$employee = employee::find($eid);

		return view('backend.employee.edit',[
            'employee' => $employee,
            'designation' => $designation,
            'branch' => $branch
		]);
    }
    
    public function postEditEmployee(Request $request, $eid){

        $employee  =  employee::findorfail($eid);
        $status = $employee->update($request->all());
        
        if($status)
            
            return redirect('/manage-employee')->with('success' , 'Employee Record Updated successfully');
		else
		    
        return redirect('/manage-employee')->with('error' , 'Sorry something went wrong contact IT');
    }



    public function deleteEmployee($eid){
        
        $employee = employee::destroy($eid);
        if ($employee){
            session()->flash('success','Staff Record Deleted successfully');
        }else{
            session()->flash('error','Sorry something went wrong');

        }
        return redirect()->back();
    }

    public function transferEmployee(){
        $branch = branch::all();
        $employee = employee::where('status','Awaiting')->paginate(7);
        // ->orWhere('status', 'Approved')->paginate(7);
        return view ('backend.transfers.new',[
            'branch'   => $branch,
            'employee' => $employee
            ]);
    }
    
    public function transferEmployeeDetails($eid){
        $branch = branch::all();
        $designation = designation::all();

        $employee = employee::findorfail($eid);
        return view ('backend.transfers.details',[
            'branch'   => $branch,
            'designation' => $designation,

            'employee' => $employee
            ]);
    }

    public function postTransferEmployee(Request $request, $eid){

        try{

            DB::beginTransaction();

                $employee = employee::findorfail($eid);
                $employee->status='Transferred';
                $employee->bid = $request->input('bid');
                $employee->save();

                $e_fname = str_slug($employee->fname);
                $e_lname = str_slug($employee->lname);
                $fullname = strtoupper($e_fname . " " . $e_lname);

                $transferhistory                        = new transferhistory();
                $transferhistory->tid                  	= $employee->eid;
                $transferhistory->bid                  	= $employee->bid;
                $transferhistory->fullname              = $fullname;
                $transferhistory->email                 = $employee->email;
                $transferhistory->currentBranch     	= $request->input('currentBranch');
                $transferhistory->reason    			= $request->input('reason');
                $transferhistory->status                = $employee->status;
                
                
                //this are only needed when employee is the one is the one making the request 
                // $transferhistory->transfer_request_date = $transfer->transfer_request_date;

                $transferhistory->save();
                                
                                
            DB::commit();

            session()->flash('success','Employee Transfered Successfully');
            return redirect()->back();
    }
    catch(\Exception $exception){
            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact IT Department.");
            
            return redirect()->back();
    }

    }

    public function getAddUser(){
        return view('backend.addUser');

    }
    public function postAddUser(Request $request){
        $user      = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt("Prymage123");
        $user->role = 'staff';
        $status = $user->save();
        
        if($status){
            return redirect()->back()->with('success','User Added Successfully');
        }else{
            return redirect()->back()->with('error','Something Went Wrong!');
        }
    
    }

    public function manageAllUsers(){
        $users = User::all();
        return view ('backend.manageUsers',[
            'users' => $users
        ]);
    }


    public function makeStaff( Request $request,$uid) {
		$user = User::findorfail($uid);
		$user->role = 'HR';
		$status =  $user->save();
		if ($status){
			session()->flash('success','Role Changed to Staff HR');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
    }
    
    public function makeAdmin( Request $request, $uid) {
		$user = User::findorfail($uid);
		$user->role = 'Admin';
		$status =  $user->save();
		if ($status){
			session()->flash('success','Role Changed to Admin');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
	}


	public function deleteUser(Request $request, $uid) {
		$user = User::destroy($uid);

		if ($user){
			session()->flash('success','User Account Deleted');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
	}


    
    public function changePassword() {
		return view('changePassword');
	}



    public function postChangePassword( Request $request ) {
		$oldpassword = $request->input('oldpassword');
		$password = $request->input('password');
		$confirmpassword = $request->input('confirmpassword');
		$user = User::find(Auth::user()->uid);

		if(password_verify($oldpassword,$user->password) ){

			if($confirmpassword == $password){
				$user->password = bcrypt($password);
				$request->session()->flash("success","Password changed successfully");
				$user->save();
				return redirect('/change-password');

			}
			else{
				$request->session()->flash("error", "Both new passwords don't match. Please try again");
				return redirect('/change-password');
			}

		} else {
			$request->session()->flash("error", "Current Password is wrong. Please try again.");
			return redirect('/change-password');
		}

    }
    


    public function logout() {
		auth()->logout();
		// session()->flash('success',"You have been logged out.");
		return redirect('/')->with('success', "You've Logged Out Successfully.");
    }

    










    
}
