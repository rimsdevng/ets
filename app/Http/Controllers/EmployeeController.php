<?php

namespace App\Http\Controllers;

use App\User;
use App\branch;
use App\designation;
use App\employee;
use App\transfer;
use App\transferhistory;
use App\department;
use Faker\Provider\Company;
use Illuminate\Http\Request;
use http\Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;


class EmployeeController extends Controller
{
		//
		
		public function __construct()
    {
        $this->middleware('auth');
    }

	// $employee->uid = Auth::user()->uid;
    public function postAddRequest(Request $request){

			$transfer = new transfer();
			$transfer->bid = $request->input('bid'); //this is the desired branch
			$transfer->fullname = $request->input('fullname');
			$transfer->email = $request->input('email');
			$transfer->currentBranch = $request->input('currentBranch');
			$transfer->reason = $request->input('reason');
			$transfer->status = 'Pending';
			$transfer->transfer_request_date = $request->input('transfer_request_date');
		
			$status = $transfer->save();

			if ($status){
				session()->flash('success','Request Created Successfully.');
			}else{
				session()->flash('error','Something went Wrong, Try again');
			}

			return redirect()->back();

		}
		
		public function approveTransferRequest(Request $request, $tid){

			try{

						DB::beginTransaction();

							$transfer = transfer::findorfail($tid);
							$transfer->status='Approved';
							$transfer->save();

							$transferhistory                        = new transferhistory();
							$transferhistory->tid                  	= $transfer->tid;
							$transferhistory->bid                  	= $transfer->bid;
							$transferhistory->fullname              = $transfer->fullname;
							$transferhistory->email                 = $transfer->email;
							$transferhistory->currentBranch     	  = $transfer->currentBranch;
							$transferhistory->reason    				    = $transfer->reason;
							$transferhistory->status                = $transfer->status;

							$transferhistory->transfer_request_date               = $transfer->transfer_request_date;
							$transferhistory->save();
											
											
						DB::commit();

						session()->flash('success','Transfer Approved Successfully');
						return redirect()->back();
				}
				catch(\Exception $exception){
						return $exception->getMessage();
						session()->flash('error',"Something went wrong. Please try again or contact IT Department.");
						
			return redirect()->back();
				}
		}


		public function disapproveRequest(Request $request, $tid){

			try{

						DB::beginTransaction();

							$transfer = transfer::findorfail($tid);
							$transfer->status='Pending';
							$transfer->save();

							$transferhistory                        = new transferhistory();
							$transferhistory->tid                  	= $transfer->tid;
							$transferhistory->bid                  	= $transfer->bid;
							$transferhistory->fullname              = $transfer->fullname;
							$transferhistory->email                 = $transfer->email;
							$transferhistory->currentBranch     	  = $transfer->currentBranch;
							$transferhistory->reason    				    = $transfer->reason;
							$transferhistory->status                = $transfer->status;

							$transferhistory->transfer_request_date               = $transfer->transfer_request_date;
							$transferhistory->save();
											
											
						DB::commit();

						session()->flash('success','Request Disqualified Successfully');
						return redirect()->back();
				}
				catch(\Exception $exception){
						return $exception->getMessage();
						session()->flash('error',"Something went wrong. Please try again or contact IT Department.");
						
			return redirect()->back();
				}
		}
}
