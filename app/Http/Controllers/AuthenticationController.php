<?php

namespace App\Http\Controllers;

use App\confirmation;
use App\Mail\forgotPasswordMail;
use App\designation;
use App\staff;
use App\employee;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthenticationController extends Controller
{


	public function __construct() {}

	public function resetPassword(Request $request) {

		$token = $request->input('token');
		$email = $request->input('email');
		$role = $request->input('role');

		if(confirmation::where('email',$email)->where('token',$token)->where('role',$role)->count() <= 0){

			session()->flash('error','Invalid Token. Please attempt to reset your password again.');
			return redirect('/');
		}

		$confirmation = confirmation::where('email',$email)->where('token',$token)->where('role',$role)->first();

		$confirmation->delete();

		return view('resetPassword',[
			'change' => true,
			'role' => $role,
			'email' => $email
		]);

	}

	public function postResetPassword(Request $request) {

		$role = $request->input('role');
		$email = $request->input('email');
		$newPassword = $request->input('newPassword');

		if($role == 'staff'){
			$staff = staff::where('email',$email)->first();
			$staff->password = bcrypt($newPassword);
			$staff->save();

			session()->flash('success','Password Reset.');
			return redirect('staff/login');

		}

	

		if($role == 'employee'){
			$employee = employee::where('phone',$email)->first();
			$employee->password = bcrypt($newPassword);
			$employee->save();
			session()->flash('success','Password Reset.');
			return redirect('employee/login');

		}

		if($role == 'admin'){
			$admin = User::where('email',$email)->first();
			$admin->password = bcrypt($newPassword);
			$admin->save();

			session()->flash('success','Password Reset.');
			return redirect('login');
		}


		return redirect('/');

	}


	//employee Authentication
	



// login employee

//Staff Authentication
public function loginemployee() {
	if(session()->has('employee')) return redirect('/');

	return view('dashboard.employee.login');
}

public function postLoginemployee( Request $request ) {
	$email = $request->email;
	$password = $request->password;

	$employee =  employee::where('email',$email)->get();

	if(count($employee) <=  0){ // check that the email exists
		session()->flash('error',"Account doesn't exist");
		return redirect()->back();
	}

	$employee =  employee::where('email',$email)->first();

	if (password_verify($password,$employee->password)){
		$employee->role = 'employee';
		// $employee->Desgination;
		session()->put('employee',$employee);

		return redirect('/');
	}else{
		session()->flash('error','Password is incorrect');
		return redirect()->back();
	}

}

















	//Staff Authentication
	public function loginStaff() {
		if(session()->has('staff')) return redirect('staff/dashboard');

		return view('dashboard.staff.login');
	}

	public function postLoginStaff( Request $request ) {
		$email = $request->email;
		$password = $request->password;

		$staff =  staff::where('email',$email)->get();

		if(count($staff) <=  0){ // check that the email exists
			session()->flash('error',"Account doesn't exist");
			return redirect()->back();
		}

		$staff =  staff::where('email',$email)->first();

		if (password_verify($password,$staff->password)){
			$staff->role = 'staff';
			$staff->Desgination;
			session()->put('staff',$staff);

			return redirect('staff/dashboard');
		}else{
			session()->flash('error','Password is incorrect');
			return redirect()->back();
		}

	}


	public function logOutUser(Request $request,$user ) {
		 session()->remove($user);
		 if ($user == 'staff'){
			 return redirect('staff/login');

		 }elseif ($user == 'employee'){
			 return redirect('employee/login');
		 }else{
			return redirect('/login');
		 }
	}


	public function changePassword() {

		if(auth()->check()) return view('changePassword');
		if(session()->has('employee')) return view('dashboard.employee.changePassword');
		if(session()->has('staff')) return view('dashboard.staff.changePassword');
		return redirect('/');
	}


	public function postChangePassword( Request $request ) {
		$currentPassword = $request->input('currentPassword');
		$newPassword = $request->input('newPassword');
		$confirmPassword = $request->input('confirmPassword');

		if($newPassword != $confirmPassword){
			session()->flash('error','Confirmed password does not match your new password. Please try again.');
			return redirect()->back();
		}



		if(auth()->check())
		{
			$user = User::find(auth()->user()->uid);

			if(!password_verify($currentPassword, $user->password)) {
				session()->flash('error','Current Password is wrong');
				return redirect()->back();
			}

			$user->password = bcrypt($newPassword);
			$user->save();
			session()->flash('success','Password Changed.');
			return redirect()->back();
		}


		if(session()->has('employee')){
			$csid = session()->get('employee')->csid;
			$employee = employee::find($csid);

			if(!password_verify($currentPassword, $employee->password)) {
				session()->flash('error','Current Password is wrong');
				return redirect()->back();
			}

			$employee->password = bcrypt($newPassword);
			$employee->save();
			session()->flash('success','Password Changed.');
			return redirect()->back();
		}

		if(session()->has('staff')){
			$stid = session()->get('staff')->stid;
			$staff = staff::find($stid);

			if(!password_verify($currentPassword, $staff->password)) {
				session()->flash('error','Current Password is wrong');
				return redirect()->back();
			}

			$staff->password = bcrypt($newPassword);
			$staff->save();
			session()->flash('success','Password Changed.');
			return redirect()->back();
		}



		return redirect()->back();
	}




	public function staffForgotPassword() {
		return view('dashboard.staff.forgotPassword');
	}

	public function postStaffForgotPassword(Request $request) {
		$email = $request->input('email');
		$token = Str::random(24);

		$confirmation = new confirmation();
		$confirmation->email = $email;
		$confirmation->token = $token;
		$confirmation->role = "staff";
		$confirmation->save();


		Mail::to($email)->send(new forgotPasswordMail($token, $email,"staff"));
		session()->flash('success','Check your mail for a password reset link.');
		return redirect()->back();
	}


	public function studentForgotPassword() {
		return view('dashboard.student.forgotPassword');
	}

	public function postStudentForgotPassword(Request $request) {
		$email = $request->input('email');
		$token = Str::random(24);

		$confirmation = new confirmation();
		$confirmation->email = $email;
		$confirmation->token = $token;
		$confirmation->role = "student";
		$confirmation->save();


		Mail::to($email)->send(new forgotPasswordMail($token, $email,"student"));
		session()->flash('success','Check your mail for a password reset link.');
		return redirect()->back();
	}


	public function employeeForgotPassword() {
		return view('dashboard.employee.forgotPassword');
	}

	public function postemployeeForgotPassword(Request $request) {
		$phone = $request->input('email');
		$token = strtoupper(self::random_number(6));


		$confirmation = new confirmation();
		$confirmation->email = $phone;
		$confirmation->token = $token;
		$confirmation->role = "employee";
		$confirmation->save();

//		Mail::to($email)->send(new forgotPasswordMail($token, $email,"student"));

		$message = "Your password reset code is $token";
		$this->sendSms($phone,$message);
		session()->flash('success','Check your phone for a password reset code.');
		session()->flash('showReset',"true");
		session()->flash('resetPhone',$phone);
		return redirect()->back();
	}

	public function adminForgotPassword() {
		return view('forgotPassword');
	}

	public function postAdminForgotPassword(Request $request) {
		$email = $request->input('email');
		$token = Str::random(24);

		$confirmation = new confirmation();
		$confirmation->email = $email;
		$confirmation->token = $token;
		$confirmation->role = "admin";
		$confirmation->save();


		Mail::to($email)->send(new forgotPasswordMail($token, $email,"admin"));
		session()->flash('success','Check your mail for a password reset link.');
		return redirect()->back();
	}


	static  function random_number($length, $keyspace = '0123456789')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[random_int(0, $max)];
		}
		return $str;
	}


	function sendSms($phone,$message) {


		$message = urlencode( $message );
		file_get_contents( "https://www.bulksmsnigeria.com/api/v1/sms/create?dnd=2&api_token=rVY7mjk9AfG2CCx9KdzHkqB1CSVCoyOvNxEvKLdnhEVbtrtcZ7uM8ElPeC7S&from=HENDON&to=$phone&body=$message" );
	}




	}
