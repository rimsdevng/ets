<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class department extends Model
{
    //
    protected $primaryKey = 'did';
	protected $table = 'departments';
	protected $guarded = [];
}
