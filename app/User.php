<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $guarded = [];
    protected $primaryKey = 'uid';
 
    protected $fillable = [
        'name', 'email', 'password', 'bid',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function branch() {
        // return $this->belongsTo('App\branch', 'bid');

        return $this->belongsTo(branch::class, 'bid');
    }
  
}
