<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class branch extends Model
{
    //
    
    protected $primaryKey = 'bid';
	protected $table = 'branches';
    protected $guarded = [];
    
    public function employee() {
        return $this->hasMany(employee::class,'bid','bid');
        
    }

    public function user() {
        // return $this->hasMany(user::class,'bid','bid');

        return $this->hasMany(User::class);

        
    }
}
