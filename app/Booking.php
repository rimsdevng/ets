<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    //
    protected  $primaryKey = 'bid';

    protected $table = 'bookings';
    protected $guarded = [ ];
}
