<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class designation extends Model
{
    //
    

    protected $primaryKey = 'deid';
	protected $table = 'designations';
    protected $guarded = [];
    
    public function employee() {
	  	return $this->hasMany(employee::class,'deid','deid');
    }
}
