<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendee extends Model
{
    //
    protected  $primaryKey = 'aid';

    protected $table = 'attendees';
    protected $guarded = [];
}
