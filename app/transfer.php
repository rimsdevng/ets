<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transfer extends Model
{
    //
    protected $primaryKey = 'tid';
	protected $table = 'transfers';
	protected $guarded = [];
}
