<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    //

    protected $primaryKey = 'eid';
	protected $table = 'employees';
    protected $guarded = [];
    
    public function designation(  ) {
		return $this->belongsTo(designation::class,'deid');
    }

    public function branch() {
      // return $this->belongsTo(branch::class,'bid');
      return $this->belongsTo('App\branch', 'bid');
    }

}
