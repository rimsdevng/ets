<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'PublicController@getWelcome');
Route::get('/branchName/{bid}', 'PublicController@getBranch');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('logout','HomeController@logout');


// post transfer request 
Route::post('/post-add-request', 'EmployeeController@postAddRequest');

Route::get('/manage-transfer', 'HomeController@manageTransferRequests');
Route::get('approve-request/{tid}','EmployeeController@approveTransferRequest');
Route::get('/disapprove-request/{tid}','EmployeeController@disapproveRequest');
Route::get('/get-transfer-reports', 'HomeController@getTransferReports');

Route::get('/transfer-employee','HomeController@transferEmployee');
Route::get('transfer-employee-detail/{eid}', 'HomeController@transferEmployeeDetails');
Route::post('post-transfer-employee/{eid}','HomeController@postTransferEmployee');

Route::group(['middleware' => 'Admin'], function(){

    Route::get('/add-branch', 'HomeController@getAddBranch');
    Route::get('delete-branch/{bid}', 'HomeController@deleteBranch');
    Route::get('/manage-branch','HomeController@manageBranch');
    Route::get('edit-branch/{bid}', 'HomeController@editbranch'); 

    Route::post('/post-add-branch', 'HomeController@postAddBranch');   
    Route::post('post-edit-branch/{bid}','HomeController@postEditBranch');
});

Route::get('/get-department', 'HomeController@getDepartment');
Route::post('/post-department', 'HomeController@postDepartment');

Route::get('/get-designation', 'HomeController@addDesignation');
Route::post('/post-designation', 'HomeController@postDesignation');

Route::get('/get-employee', 'HomeController@addEmployee');
Route::post('/post-employee', 'HomeController@postEmployee');
Route::get('/manage-employee', 'HomeController@manageEmployee');

Route::get('edit-employee/{eid}', 'HomeController@editEmployee');
Route::post('/post-edit-employee/{eid}','HomeController@postEditEmployee');
Route::get('delete-employee/{eid}', 'HomeController@deleteEmployee');




//Authentication for staff
Route::get('employee/login','AuthenticationController@loginEmployee');
Route::post('employee/login','AuthenticationController@postLoginEmployee');
Route::get('employee/dashboard','EmployeeController@dashboard');



Route::get('/make-customer','HomeController@makeCustomer');


Route::get('/make-booking', 'HomeController@makeBooking');
Route::post('/post-booking', 'HomeController@postBooking');

Route::get('/change-password', 'HomeController@changePassword');
Route::post('/change-password','HomeController@postChangePassword');


Route::get('/add-user','HomeController@getAddUser');
Route::post('post-add-user', 'HomeController@postAddUser');

Route::get('/manage-all-users', 'HomeController@manageAllUsers');
Route::get('make-staff/{uid}', 'HomeController@makeStaff');
Route::get('make-admin/{uid}','HomeController@makeAdmin');
Route::get('delete-user/{uid}','HomeController@deleteUser');

Route::get('/employee/logins', function () {
    return view('samplelogin');
});