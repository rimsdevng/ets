<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('departments', function (Blueprint $table) {
            $table->increments('did');
            $table->string('name');
			$table->string('slug');
            $table->timestamps();
        });

        // the roles stands for positions
        Schema::create('designations', function (Blueprint $table) {
            $table->increments('deid');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });
        
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('bid');
            $table->string('name');
            $table->string('address',2000);
            $table->timestamps();
        });

        Schema::create('transfers', function (Blueprint $table) {
            $table->increments('tid');
            $table->integer('bid');
            $table->enum( 'status', ['Awaiting', 'Approved', 'Processing', 'Pending' ])->default('Pending');

            $table->string('email')->nullable()->unique();
			$table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('currentBranch')->nullable();
            $table->string('desiredBranch')->nullable();

            $table->string('reason',5000);
            $table->string('transfer_request_date')->nullable();
            $table->timestamps();
        });

        
       

        Schema::create('employees', function (Blueprint $table) {
            $table->increments('eid');
            $table->integer('deid')->nullable();
            $table->integer('bid')->nullable();

            $table->integer('did')->nullable();
			$table->string('email')->unique();
			$table->string('fname')->nullable();
			$table->string('lname')->nullable();
			$table->string('dob')->nullable();
			$table->string('employeed_date')->nullable();
			$table->string('gender')->nullable();
            $table->string('phone')->nullable();
            $table->string('password')->nullable();
			$table->string('address')->nullable();
			$table->string('town')->nullable();
			$table->string('city')->nullable();
			$table->string('country')->nullable();
			
            $table->softDeletes();
            $table->timestamps();
        });



        // create a middleware, 
        // then while creating employee data its should pick the name, email and password and add to the users table with a role of employeoe
        //then when someone want's to place a transfer request, 
        //it should pick the name of the employee in session and its current branch, 
        // current from employee table
        // then desire branch should be picked from branch table 
        // then add the submit the details to the request table database.
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        
        
        Schema::dropIfExists('departments');
        
        Schema::dropIfExists('designations');
        
        Schema::dropIfExists('branches');
    
        Schema::dropIfExists('transfers');
        
        Schema::dropIfExists('employees');
        
        

    }
}
