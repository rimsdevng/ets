<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferhistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer_histories', function (Blueprint $table) {
            $table->increments('thid');
            $table->integer('tid')->nullable();
            $table->integer('bid')->nullable();
			$table->string('fullname')->nullable();
            $table->string('email')->nullable();
            $table->string('currentBranch')->nullable();
            $table->string('reason',5000)->nullable();
            $table->string( 'status')->nullable();


            $table->string('transfer_request_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transferhistories');
    }
}
