-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 02, 2019 at 04:50 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
CREATE TABLE IF NOT EXISTS `branches` (
  `bid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`bid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`bid`, `name`, `address`, `created_at`, `updated_at`) VALUES
(2, 'McCarthy Branch', 'N1 Extension, Kasoa Winneba Road', '2019-05-12 17:02:07', '2019-05-12 17:02:07'),
(3, 'Lapaz Branch', 'N1, Lapaz Mallam Road, Accra, Ghana', '2019-05-12 17:04:12', '2019-05-30 18:08:16'),
(4, 'Darkuman Branch', 'N1 Extension', '2019-05-21 15:49:47', '2019-05-21 15:49:47'),
(5, 'Darkuman Branch', 'N1 Extension, Kasoa Winneba Road, Accra, Ghana', '2019-05-30 18:07:52', '2019-05-30 18:07:52');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `did` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`did`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

DROP TABLE IF EXISTS `designations`;
CREATE TABLE IF NOT EXISTS `designations` (
  `deid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`deid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`deid`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Customer Service Manager', 'Personnel in charge of customer services', '2019-05-12 17:33:32', '2019-05-12 17:33:32'),
(2, 'Marketing Officer', 'An Officer in charge of Marketing', '2019-05-12 17:33:56', '2019-05-12 17:33:56'),
(3, 'Branch Manager', 'Manager in charge of a particular branch', '2019-05-12 17:34:15', '2019-05-12 17:34:15'),
(4, 'Relationship Officer', 'Personnel in charge of relationship', '2019-05-12 17:34:34', '2019-05-12 17:34:34'),
(5, 'Customer Service Officer', 'Personnel who handles customer services', '2019-05-12 17:34:57', '2019-05-12 17:34:57'),
(6, 'Teller', 'Staffs who receives and give cash over the counter', '2019-05-12 17:35:22', '2019-05-12 17:35:22');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `eid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `deid` int(11) DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employeed_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `town` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bid` int(11) DEFAULT NULL,
  `status` enum('Awaiting','Transferred','Approved','Processing','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Awaiting',
  PRIMARY KEY (`eid`),
  UNIQUE KEY `employees_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`eid`, `deid`, `did`, `email`, `fname`, `lname`, `dob`, `employeed_date`, `gender`, `phone`, `address`, `town`, `city`, `country`, `deleted_at`, `created_at`, `updated_at`, `password`, `bid`, `status`) VALUES
(3, 4, 4, 'alexm@ecobank.com', 'Alex', 'Nelson', '1980-02-05', '2015-10-01', 'Female', '0238855055', 'no.31 samora machel street , Madina', 'Accra', 'Accra', 'Ghana', NULL, '2019-05-12 19:28:38', '2019-05-30 12:39:09', '$2y$10$jRdWyef/MPwPi3Z7ogO0yu5xs4EfSyrQnzt6Xcn3HUSCx8s/PxWd.', 3, 'Transferred'),
(4, 1, 2, 'nasiruibrahim@gmail.com', 'Grace', 'Ibrahim', '1990-10-20', '2015-10-01', 'Female', '0238855068', 'Behind Jeyee University Ghana', 'Accra', 'McCarthy', 'Ghana', NULL, '2019-05-24 21:47:34', '2019-05-30 12:51:00', '$2y$10$garI4GLdCC/EOBAMhFb64OKvcJ75E0siLqQpU7e0zaRI0zdHLiAA2', 4, 'Transferred'),
(5, 3, NULL, 'iboateng@ecobank.com', 'Isaac', 'Boateng', '1980-03-02', '2019-05-29', 'Male', '0230000006', 'N1 Extension', 'Accra', 'Accra', 'Ghana', NULL, '2019-05-30 16:55:54', '2019-05-30 16:57:31', '$2y$10$XobhkwWVkvc87zc.5yin/OBEZNayrKxKP6hirT/eT85FKiJdDDX2u', 3, 'Transferred');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_12_153825_initial', 1),
(4, '2019_05_12_193554_newemployee', 2),
(5, '2019_05_12_201633_newemployee', 3),
(6, '2019_05_12_202037_add_to_employees_table', 4),
(7, '2019_05_12_203117_add_to_employees_table', 5),
(8, '2019_05_23_152010_add_to_transfers_table', 6),
(9, '2019_05_26_133507_add_to_users_table', 7),
(10, '2019_05_29_111941_create_transferhistories_table', 8),
(11, '2019_05_30_081750_add_to_employees_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transfers`
--

DROP TABLE IF EXISTS `transfers`;
CREATE TABLE IF NOT EXISTS `transfers` (
  `tid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bid` int(11) NOT NULL,
  `status` enum('Awaiting','Approved','Processing','Pending') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `reason` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transfer_request_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currentBranch` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`tid`),
  UNIQUE KEY `transfers_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transfers`
--

INSERT INTO `transfers` (`tid`, `bid`, `status`, `reason`, `transfer_request_date`, `created_at`, `updated_at`, `email`, `fullname`, `currentBranch`) VALUES
(4, 3, 'Approved', 'Hello', '2019-06-27', '2019-05-29 09:37:59', '2019-05-30 17:02:16', 'elisgeneral@gmail.com', 'Elis General', 'Darkuman Branch'),
(5, 1, 'Pending', 'Health Reason', '2019-07-30', '2019-05-30 17:10:27', '2019-05-30 17:10:27', 'iboateng@ecobank.com', 'ISAAC BOATENG', 'McCarthy Branch');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_histories`
--

DROP TABLE IF EXISTS `transfer_histories`;
CREATE TABLE IF NOT EXISTS `transfer_histories` (
  `thid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tid` int(11) DEFAULT NULL,
  `bid` int(11) DEFAULT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currentBranch` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transfer_request_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`thid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transfer_histories`
--

INSERT INTO `transfer_histories` (`thid`, `tid`, `bid`, `fullname`, `email`, `currentBranch`, `reason`, `status`, `transfer_request_date`, `created_at`, `updated_at`) VALUES
(1, 4, 3, 'Elis General', 'elisgeneral@gmail.com', 'Darkuman Branch', 'Hello', 'Approved', '2019-06-27', '2019-05-29 11:40:21', '2019-05-29 11:40:21'),
(4, 4, 3, 'Elis General', 'elisgeneral@gmail.com', 'Darkuman Branch', 'Hello', 'Pending', '2019-06-27', '2019-05-29 12:15:42', '2019-05-29 12:15:42'),
(9, 4, 3, 'Elis General', 'elisgeneral@gmail.com', 'Darkuman Branch', 'Hello', 'Approved', '2019-06-27', '2019-05-30 17:02:16', '2019-05-30 17:02:16'),
(8, 5, 3, 'ISAAC BOATENG', 'iboateng@ecobank.com', 'McCarthy Branch', 'Family Reason', 'Transferred', NULL, '2019-05-30 16:57:31', '2019-05-30 16:57:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('Admin','HR','Employee') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Employee',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bid` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`, `bid`) VALUES
(1, 'Elis General', 'elisgeneral@ecobank.com', NULL, '$2y$10$VggiXjdSmISFTzoQUtQV2.1gEsrfOfFyCZzOW0E4WaRz/KY6uloFK', 'HR', 'hmqXfZGAlb62yT8liBK6n3NdCt5p9MLf9u90NDJkVBj7CND42hp5J8uSluUW', '2019-05-12 15:40:21', '2019-05-30 16:24:39', 1),
(2, 'FELIX MENSAH', 'fmensah@ecobank.com', NULL, '$2y$10$U5pcNVG./gn799IeqXwTguqkC0Z4T4oC9YP1wlch/bgA6.iQZ6JL6', 'Employee', 'tb7w6ah4QH7wQTtwDvUn3F2Y34viVbG1MXQK7wskOHK5Ierr6UvAIkanZQVM', '2019-05-12 19:25:51', '2019-05-12 19:25:51', 2),
(3, 'ALEX NELSON', 'alexm@ecobank.com', NULL, '$2y$10$jRdWyef/MPwPi3Z7ogO0yu5xs4EfSyrQnzt6Xcn3HUSCx8s/PxWd.', 'Admin', 'YXwPia5x1qMSxYyfWrY1G0gdyQWos0I3f84M4x2bqE216Duzebci0lU7Js1h', '2019-05-12 19:28:38', '2019-05-12 19:28:38', 3),
(4, 'NASIRU IBRAHIM', 'nasiruibrahim@gmail.com', NULL, '$2y$10$garI4GLdCC/EOBAMhFb64OKvcJ75E0siLqQpU7e0zaRI0zdHLiAA2', 'Employee', NULL, '2019-05-24 21:47:34', '2019-05-24 21:47:34', 4),
(5, 'ISAAC BOATENG', 'iboateng@ecobank.com', NULL, '$2y$10$XobhkwWVkvc87zc.5yin/OBEZNayrKxKP6hirT/eT85FKiJdDDX2u', 'Employee', 'psrUvlb2kIOxAmxYl9fyYdDiCq3ZZPlBaraOELN28jruQBjkElg8WatwYL0J', '2019-05-30 16:55:54', '2019-05-30 16:55:54', 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
